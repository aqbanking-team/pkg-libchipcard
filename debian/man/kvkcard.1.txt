KVKCARD(1)
==========
:author:  Micha Lenk
:email:   micha@debian.org
:revdate: 2016-02-15

NAME
----
kvkcard - a commandline client to read information from a German medical card

SYNOPSIS
--------
'kvkcard' [command] [options]

DESCRIPTION
-----------
'kvkcard' is a commandline tool to read information from a German medical card (KVK or eGK).

COMMANDS
--------

read::
    read data from a German medical card.

OPTIONS
-------

-v::
--verbous::
    Every occurrence of this option increases the verbosity.

-f OUTFILE::
--filename=OUTFILE::
    File to write to. If omitted stdout will be used.

-b::
--beep::
    Beep after reading a card.

-i INFILE::
--infilename=INFILE::
    File to read from. Only needed for psvd and pspd.

-d::
--dosmode::
    Store data in DOS mode

-p PARAM::
--program=PARAM::
    Program to call on cards found.

-a ARGS::
--args=ARGS::
    Arguments for the program to be called

SEE ALSO
--------

http://www.libchipcard.de::
    Homepage of libchipcard project.

/usr/share/doc/libchipcard-tools/
    Local documentation
