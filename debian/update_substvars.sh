#!/bin/sh
set -e
set -x

CHIPCARD_SONAME=$(sed -n "s/LIBCHIPCARD_SO_EFFECTIVE='\([^']*\)'$/\1/p" config.log)
if [ -z "$CHIPCARD_SONAME" ]; then
	echo "Couldn't detect LibChipcard's SONAME" >& 2
	exit 1
fi

GWEN_SONAME=$(sed -n "s,^gwenhywfar_plugins='.*/\([^/]*\)',\1,p" config.log)
if [ -z "$GWEN_SONAME" ]; then
	echo "Couldn't detect Gwenhywfar's SONAME" >& 2
	exit 1
fi

SUBSTVARS_FILE="debian/libchipcard${CHIPCARD_SONAME}t64.substvars"
echo "libgwenhywfar:SONAME=$GWEN_SONAME" >> $SUBSTVARS_FILE
